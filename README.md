# self-hosted catalog

This repository provides a catalog of applications intended to be deployed to
kubernetes clusters using kustomize (or `kubectl apply -k`), using gitops methodologies.

## Goals

* wherever possible, run on ARM or x86 interchangeably.
* provide kustomization [bases](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#base) that
  leave as many decisions as possible to overlays.
* aim for non-HA bases, provide HA overlay where possible.
* reuse resources and assets made available by projects wherever possible.

